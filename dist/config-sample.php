<?php
define('TENON_API_KEY', 'this is where you enter your api key');
define('TENON_API_URL', 'http://www.tenon.io/api/');
define('HASH_FILEPATH', $_SERVER['DOCUMENT_ROOT'] . '/src/hashes.txt');
define('CSV_FILE_PATH', $_SERVER['DOCUMENT_ROOT'] . '/csv/');
define('CSV_FILE_NAME', 'tenon.csv');
define('CSV_FILE_MODE', 'a+');
define('DEBUG', false);
