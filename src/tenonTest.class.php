<?php

/**
 *    This class submits a request against the Tenon API for automatic
 *    accessibility testing.
 *
 */
class tenonTest
{
    protected $url, $opts, $hash;
    public $tenonResponse, $tURL;

    /**
     * Class constructor
     *
     * @param   string $url  the API url to post your request to
     * @param    array $opts options for the request
     */
    public function __construct($url, $opts)
    {
        $this->url = $url;
        $this->opts = $opts;
        $this->hash = md5(serialize($opts['src']));
        $this->tenonResponse = '';
    }

    /**
     * Submits the HTML source for testing
     *
     * @param   bool $printInfo whether or not to print the output from curl_getinfo (usually for debugging only)
     *
     * @return    string    the results, formatted as JSON
     */
    public function submit($printInfo = false)
    {

        if (true == $printInfo) {
            echo '<h2>Options Passed To TenonTest</h2><pre><br>';
            var_dump($this->opts);
            echo '</pre>';
        }

        //open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->opts);

        //execute post and get results
        $result = curl_exec($ch);

        if (true == $printInfo) {
            echo 'ERROR INFO (if any): ' . curl_error($ch) . '<br>';
            echo '<h2>Curl Info </h2><pre><br>';
            print_r(curl_getinfo($ch));
            echo '</pre>';
        }

        $this->tCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        //the test results
        $this->tenonResponse = $result;

    }

    /**
     *
     * @param     $array
     * @param int $minlength
     *
     * @return array|bool
     */
    protected function cleanArray($array, $minlength = 32)
    {
        $output = array();

        if (!is_array($array)) {
            return false;
        }
        foreach ($array AS $val) {
            if (strlen($val) < $minlength) {
                continue;
            } else {
                $output[] = trim($val);
            }
        }

        return $output;
    }


    /**
     *
     *
     * @return bool
     */
    public function hashExists()
    {
        if (file_exists(HASH_FILEPATH)) {
            $array = file(HASH_FILEPATH);

            $array = $this->cleanArray($array);

            if (false == in_array(trim($this->hash), $array, true)) {
                $handle = fopen(HASH_FILEPATH, 'a+');

                if (false != $handle) {
                    fwrite($handle, $this->hash . PHP_EOL);
                    fclose($handle);
                }

                return false;
            } else {
                return true;
            }
        }
    }


    /**
     * @return mixed
     */
    public function decodeResponse()
    {
        if ((false !== $this->tenonResponse) && (!is_null($this->tenonResponse))) {
            $result = json_decode($this->tenonResponse, true);
            if (!is_null($result)) {
                $this->rspArray = $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     *
     * @param        $pathToFolder
     * @param null   $name
     * @param string $mode
     *
     * @return bool
     */
    public function writeResultsToCSV($pathToFolder, $name = null, $mode = 'a+')
    {
        $this->decodeResponse();

        $issues = $this->rspArray['resultSet'];

        if (is_null($name)) {
            $name = htmlspecialchars($this->rspArray['request']['docID']) . '.csv';
        }

        $count = count($issues);

        if ($count < 1) {
            return false;
        }

        for ($x = 0; $x < $count; $x++) {
            $rows[$x] = array(
                $this->tURL,
                $issues[$x]['tID'],
                $issues[$x]['resultTitle'],
                $issues[$x]['errorTitle'],
                $issues[$x]['errorDescription'],
                implode(', ', $issues[$x]['standards']),
                html_entity_decode($issues[$x]['errorSnippet']),
                $issues[$x]['position']['line'],
                $issues[$x]['position']['column'],
                $issues[$x]['xpath'],
                $issues[$x]['certainty'],
                $issues[$x]['priority'],
                $issues[$x]['ref'],
                $issues[$x]['signature']
            );
        }

        // Put a row of headers up on the beginning
        array_unshift($rows, array('URL', 'testID', 'Best Practice', 'Issue Title', 'Description',
            'WCAG SC', 'Issue Code', 'Line', 'Column', 'xPath', 'Certainty', 'Priority', 'Reference', 'Signature'));


        $fp = fopen($pathToFolder . $name, $mode);

        if (false != $fp) {
            foreach ($rows as $fields) {
                fputcsv($fp, $fields);
            }
            fclose($fp);

            return true;
        }

        return false;
    }
}